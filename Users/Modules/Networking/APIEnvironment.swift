//
//  UITableView+Extensions.swift
//  Users
//
//  Created by Arun Joseph on 03/09/21.
//

enum ApiEnvironment {
    
    static let current: ApiEnvironment = .development
    
    case development
    case production
    
    var baseURL: String {
        switch self {
        case .development:
            return "https://jsonplaceholder.typicode.com/"
        case .production:
            return ""
        }
    }
}

