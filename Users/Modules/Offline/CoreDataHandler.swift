//
//  CoreDataHandler.swift
//  Users
//
//  Created by Arun Joseph on 05/09/21.
//

import UIKit
import CoreData

typealias SaveCompletion = (_ status: Bool, _ error: Error?)-> ()

class CoreDataHandler {
    
    private let viewContext: NSManagedObjectContext!
    static let shared = CoreDataHandler()
    
    init() {
        viewContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    }
    
    //MARK:- Add to Coredata
    func add<T: NSManagedObject> (_ type: T.Type)-> T? {
        guard let entityName = T.entity().name, let entity = NSEntityDescription.entity(forEntityName: entityName, in: viewContext) else {return nil}
        let object = T(entity: entity, insertInto: viewContext)
        return object
    }
    
    //MARK:- Fetch
    func fetch<T: NSManagedObject> (_ type: T.Type)-> [T]? {
        let request = T.fetchRequest()
        do{
            let result = try viewContext.fetch(request)
            return result as? [T]
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    //MARK:- Save
    func save(completion: @escaping SaveCompletion) {
        do{
            try viewContext.save()
            completion(true,nil)
        } catch {
            print(error.localizedDescription)
            completion(false,error)
        }
    }
}
