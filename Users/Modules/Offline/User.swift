//
//  User.swift
//  Users
//
//  Created by Arun Joseph on 05/09/21.
//

import Foundation
import CoreData
 
public class User: NSManagedObject {
    @NSManaged var name: String
    @NSManaged var id: Int16
    @NSManaged var username: String
    @NSManaged var email: String
    @NSManaged var phone: String
    @NSManaged var website: String
    @NSManaged var address: String
    @NSManaged var companyName: String
    @NSManaged var latitude: String
    @NSManaged var longitude: String
}
