//
//  UserServerModel.swift
//  Users
//
//  Created by Arun Joseph on 03/09/21.
//

import Foundation

typealias StoreCompletion = (_ status: Bool, _ error: Error?)->()


struct UserServerModel: Decodable {

    var id: Int
    var name: String
    var username: String
    var email: String
    var phone: String
    var website: String
    var address: Address
    var company: Company
    
    static let coreDataHandler = CoreDataHandler.shared
    
    func store(completion: @escaping StoreCompletion) {
        guard let user = UserServerModel.coreDataHandler.add(User.self) else{return}
        user.address = getAddress(address: address)
        user.id = Int16(id)
        user.name = name
        user.username = username
        user.email = email
        user.phone = phone
        user.website = website
        user.companyName = company.name
        UserServerModel.coreDataHandler.save { saved, error in
            if let error = error {
                completion(false, error)
            } else{
                if saved {
                    completion(true, nil)
                }
            }
        }
    }
    
    private func getAddress(address: Address)-> String {
        return address.street + ", " + address.suite + ", " + address.city + ", " + address.zipcode
    }
}

struct Address: Decodable {
    var street: String
    var suite: String
    var city: String
    var zipcode: String
    var geo: Geo
}

struct Geo: Decodable {
    var lat: String
    var lng: String
}

struct Company: Decodable {
    var name: String
    var catchPhrase: String
    var bs: String
}
