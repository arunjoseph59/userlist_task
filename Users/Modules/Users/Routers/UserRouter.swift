//
//  UserRouter.swift
//  Users
//
//  Created Arun Joseph on 04/09/21.
//

import Foundation
import Alamofire

enum UserRouter: URLRequestBuilder {
    
    case getUserList
    
    var endpoint: EndPoint {
        switch self {
        case .getUserList: return .getusers
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .getUserList: return .get
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let request = baseRequest
        return request
    }
}
