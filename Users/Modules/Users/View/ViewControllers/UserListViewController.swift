//
//  UserListViewController.swift
//  Users
//
//  Created by Arun Joseph on 03/09/21.
//

import UIKit

class UserListViewController: BaseViewController {

    @IBOutlet weak var userListTableView: UITableView!
    
    private let viewModel = UserViewModel()
    
    //MARK:- Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAttributes()
        userListTableView.register(cell: UserTableViewCell.self)
        viewModel.delegate = self
        fetchUsers()
    }
    
    private func fetchUsers() {
        self.activityStartAnimating()
        viewModel.fetch()
    }
    
    //MARK:- Setup Attributes
    private func setupAttributes() {
        userListTableView.separatorStyle = .none
        self.title = Constants.ViewControllerName.users
    }
    
    private func navigateToDetails(with user: User) {
        let userDetailsViewController = UserDetailsViewController.instantiate()
        userDetailsViewController.title = Constants.ViewControllerName.userDetails
        userDetailsViewController.viewModel = UserDetailsViewModel(user: user)
        self.navigationController?.pushViewController(userDetailsViewController, animated: true)
    }
}

//MARK:- UITableViewDataSource, UITableViewDelegate
extension UserListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.userList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = userListTableView.dequeue(cell: UserTableViewCell.self, forIndexPath: indexPath)
        guard let userList = viewModel.userList else {return cell}
        cell.nameLabel.text = userList[indexPath.row].name
        cell.numberLabel.text = userList[indexPath.row].phone
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let userList = viewModel.userList else{return}
        navigateToDetails(with: userList[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

//MARK:- UserViewModelDelegate
extension UserListViewController: UserViewModelDelegate {
    func didFetchUser() {
        self.activityStopAnimating()
        self.userListTableView.reloadData()
    }
    
    func didFailedwith(error: String) {
        self.activityStopAnimating()
        self.showAlert(message: error, title: Constants.AlertTitle.error, buttonTitle: Constants.AlertButtonTitle.ok)
    }
}

