//
//  UserDetailsViewController.swift
//  Users
//
//  Created by Arun Joseph on 03/09/21.
//

import UIKit

class UserDetailsViewController: BaseViewController, Storyboarded {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var company: UILabel!
    
    static var storyboardName: String {Constants.Storyboards.main}
    var viewModel: UserDetailsViewModel?
    
    //MARK:- Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateWithSelectedUser()
    }
    
    func updateWithSelectedUser() {
        name.text = viewModel?.user?.name
        phone.text = viewModel?.user?.phone
        email.text = viewModel?.user?.email
        address.text = viewModel?.getUserAddress()
        company.text = viewModel?.user?.companyName
    }
}
