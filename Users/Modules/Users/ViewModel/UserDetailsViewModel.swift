//
//  UserDetailsViewModel.swift
//  Users
//
//  Created by Arun Joseph on 04/09/21.
//

import Foundation

class UserDetailsViewModel {
    
    var user: User?
    
    init(user: User) {
        self.user = user
    }
    
    //MARK:- Get user address
    func getUserAddress()-> String {
        guard let user = user else{return ""}
        return user.address
    }
}
