//
//  UserViewModel.swift
//  Users
//
//  Created by Arun Joseph on 03/09/21.
//

import Foundation

typealias FetchCompletion = (_ userList: [User]?, _ error: Error?)-> ()

protocol UserViewModelDelegate: AnyObject {
    func didFetchUser()
    func didFailedwith(error: String)
}

class UserViewModel {
    
    private let apiManager = ApiManager()
    weak var delegate: UserViewModelDelegate?
    var userList: [User]? {
        didSet{
            DispatchQueue.main.async {
                self.delegate?.didFetchUser()
            }
        }
    }
    let coreDataHandler = CoreDataHandler.shared
    
    //MARK:- Fetch
    func fetch() {
        if NetworkReachability.isConnectedToInternet() {
            fetchUserList()
        }
        else{
            fetchFromLocal {[weak self] userList, error in
                if let error = error {
                    self?.delegate?.didFailedwith(error: error.localizedDescription)
                }
                else{
                    guard let userList = userList else {
                        self?.delegate?.didFailedwith(error: CustomError.unknownError.localizedDescription)
                        return}
                    userList.count > 0 ? self?.userList = userList: self?.delegate?.didFailedwith(error: CustomError.noUsers.localizedDescription)
                }
            }
        }
    }
    
    //MARK:- Fetch users
    private func fetchUserList() {
        let request = UserRouter.getUserList
        apiManager.request(request, responseType: [UserServerModel].self) { [weak self] result in
            switch result {
            case .success(let userList):
                self?.saveLocally(userList: userList, completion: { saved, error in
                    if let error = error {
                        self?.delegate?.didFailedwith(error: error.localizedDescription)
                    }
                    else{
                        if saved {
                            self?.fetchFromLocal(completion: { userList, error in
                                if let error = error {
                                    self?.delegate?.didFailedwith(error: error.localizedDescription)
                                } else{
                                    guard let userList = userList else{
                                        self?.delegate?.didFailedwith(error: CustomError.unknownError.localizedDescription)
                                        return}
                                    self?.userList = userList
                                }
                            })
                        }
                    }
                })
            case .failure(let error):
                DispatchQueue.main.async {
                    self?.delegate?.didFailedwith(error: error.localizedDescription)
                }
            }
        }
    }
    
    //MARK:- Fetch from Local
    func fetchFromLocal(completion: @escaping FetchCompletion) {
        guard let userList = coreDataHandler.fetch(User.self) else{
            completion(nil, CustomError.unableToFetch)
            return}
        completion(userList, nil)
    }
    
    //MARK:- Save Locally
    private func saveLocally(userList: [UserServerModel], completion: @escaping SaveCompletion) {
        userList.forEach({$0.store { saved, error in
            if let error = error {
                completion(false, error)
                return
            }
        }})
        completion(true,nil)
    }
}
