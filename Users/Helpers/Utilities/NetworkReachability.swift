//
//  NetworkReachability.swift
//  Users
//
//  Created by Arun Joseph on 05/09/21.
//

import Foundation

import Alamofire

class NetworkReachability {
    class func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
}
