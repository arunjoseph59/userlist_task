//
//  CustomError.swift
//  Users
//
//  Created by Arun Joseph on 05/09/21.
//

import Foundation

enum CustomError: Error {
    case unknownError
    case unableToFetch
    case noUsers
}

extension CustomError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        
        case .unknownError:
            return Constants.ErrorDescription.unKnownError
        case .unableToFetch:
            return Constants.ErrorDescription.unableToFetch
        case .noUsers:
            return Constants.ErrorDescription.noUsers
        }
    }
}
