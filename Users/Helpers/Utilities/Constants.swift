//
//  Constants.swift
//  Users
//
//  Created by Arun Joseph on 03/09/21.
//

import Foundation

struct Constants{
    
    struct ViewControllerName{
        static let users = "Users"
        static let userDetails = "Details"
    }
    
    struct Storyboards{
        static let main = "Main"
    }
    
    struct AlertTitle {
        static let appName = "User List"
        static let error = "Error"
        static let info = "Info"
    }
    
    struct AlertButtonTitle {
        static let ok = "Ok"
        static let cance = "Cancel"
        static let retry = "Retry"
    }
    
    struct ErrorDescription {
        static let unKnownError = "Unknown error"
        static let unableToFetch = "Unable to fetch data"
        static let noUsers = "No users found"
    }
}
