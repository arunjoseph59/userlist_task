//
//  BaseViewController.swift
//  Users
//
//  Created by Arun Joseph on 04/09/21.
//

import UIKit

class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setupNavgatoinBar()
    }
}
