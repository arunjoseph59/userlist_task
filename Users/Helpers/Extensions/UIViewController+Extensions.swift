//
//  UIViewController+Extensions.swift
//  Users
//
//  Created by Arun Joseph on 05/09/21.
//

import UIKit

extension UIViewController {
    
    //MARK:- Show alert
    func showAlert(message: String, title: String, buttonTitle: String) {
        DispatchQueue.main.async(execute: {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: buttonTitle, style: .default) { _ in }
            
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
        })
    }
    
    //MARK:- Activty Start Animating
    func activityStartAnimating() {
        DispatchQueue.main.async {
            let backgroundView = UIView()
            backgroundView.frame = CGRect.init(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
            backgroundView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
            backgroundView.tag = 475647
            
            var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
            activityIndicator = UIActivityIndicatorView(frame: CGRect.init(x: 0, y: 0, width: 50, height: 50))
            activityIndicator.center = self.view.center
            activityIndicator.hidesWhenStopped = true
            activityIndicator.style = UIActivityIndicatorView.Style.medium
            activityIndicator.color = UIColor.white
            activityIndicator.startAnimating()
            self.view.isUserInteractionEnabled = false
            
            backgroundView.addSubview(activityIndicator)
            
            self.view.addSubview(backgroundView)
        }
        
    }
    
    //MARK:- Activity Stop animating
    func activityStopAnimating() {
        DispatchQueue.main.async {
            if let background = self.view.viewWithTag(475647){
                background.removeFromSuperview()
            }
            self.view.isUserInteractionEnabled = true
        }
    }
    
    
}
