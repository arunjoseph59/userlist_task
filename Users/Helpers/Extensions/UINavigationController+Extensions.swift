//
//  UINavigationController+Extensions.swift
//  Users
//
//  Created by Arun Joseph on 04/09/21.
//

import Foundation
import UIKit

extension UINavigationController {
    func setupNavgatoinBar() {
        navigationBar.barTintColor = .black
        navigationBar.tintColor = .white
        navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white, .font: UIFont.systemFont(ofSize: 20, weight: .semibold)]
    }
}
